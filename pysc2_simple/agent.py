import tensorflow as tf
import tensorflow.keras.layers as K
import numpy as np
from absl import flags

FLAGS = flags.FLAGS

flags.DEFINE_float('lr', 1e-4, 'Learning rate')
flags.DEFINE_float('gamma', 0.9999, 'Discount factor')
flags.DEFINE_float('beta', 1e-4, 'Entropy loss coeff')

flags.DEFINE_string('model_file', 'model.ckpt', 'File to save or load the model')
flags.DEFINE_bool('load_model', False, 'Load a previous model')

class Agent:
  def __init__(self, sess: tf.Session):
    self._sess = sess

  def discount_rewards(self, R):
    gamma = FLAGS.gamma

    r = np.array([gamma**i * R[i]
            for i in range(len(R))])
    r = r[::-1].cumsum()[::-1]

    return r

  def build_model(self, input_size=32, feature_idxs=[4, 9]):
    # Preporcess input
    self._input_szie = input_size
    self.observation = tf.placeholder(tf.int32, [None, 17, input_size, input_size], name='observation')

    observation = tf.cast(self.observation, tf.float32)

    inputs = [tf.expand_dims(observation[:,idx], 1) for idx in feature_idxs]
    input_screen = tf.concat(inputs, 1)

    input_screen /= 255.0

    # Very simple neural net
    reshaped = tf.transpose(input_screen, [0, 2, 3, 1])
    conv = K.Conv2D(1, 4, 1, padding='same', activation='relu')(reshaped)
    # conv = K.Conv2D(2, 8, 1, padding='same', activation='relu')(conv)
    # conv = K.Conv2D(1, 4, 1, padding='same', activation='relu')(conv)

    flat = K.Flatten()(conv)

    # fc = K.Dense(input_size**2)(flat)

    # Output
    # self.output_screen = K.Dense(input_size**2, activation='softmax')(fc)
    self.output_screen = K.Softmax()(flat)

    # Training placeholders
    self.selected_screen = tf.placeholder(tf.float32, [None, input_size**2])
    self.rewards = tf.placeholder(tf.float32, [None])
    self.learning_rate = tf.placeholder(tf.float32, ())

    log_prob = tf.log(tf.clip_by_value(tf.reduce_sum(self.selected_screen * self.output_screen, axis=1), 1e-10, 1))
    advantage = self.rewards - tf.reduce_mean(self.rewards)

    print('log_prob.shape: ', log_prob.shape)
    print('advantage.shape: ', advantage.shape)

    self.entropy_loss = -tf.reduce_sum(self.output_screen * tf.log(tf.clip_by_value(self.output_screen, 1e-10, 1)))
    self.policy_loss = -tf.reduce_sum(log_prob * advantage)

    self.loss = self.policy_loss + (FLAGS.beta * self.entropy_loss)

    self.train_op = tf.train.RMSPropOptimizer(self.learning_rate).minimize(self.loss)

    if FLAGS.load_model:
      self.load()
    else:
      self._sess.run(tf.global_variables_initializer())

  def save(self):
    saver = tf.train.Saver()
    saver.save(self._sess, FLAGS.model_file)

  def load(self):
    saver = tf.train.Saver()
    saver.restore(self._sess, FLAGS.model_file)

  def encode_action(self, act):
    oh = np.zeros(self._input_szie**2)
    oh[act] = 1.0

    return oh

  def forward(self, obs):
    return self._sess.run(self.output_screen, {
      self.observation: [obs]
    }).ravel()

  def train(self, obs, actions, rewards):
    p_loss, e_loss, _ = self._sess.run([self.policy_loss, self.entropy_loss, self.train_op], {
      self.observation: obs,
      self.selected_screen: actions,
      self.rewards: rewards,
      self.learning_rate: FLAGS.lr,
    })

    self.save()

    print('p_loss.shape: ', p_loss.shape)
    print('e_loss.shape: ', e_loss.shape)
    return p_loss.item(), e_loss.item()