from absl import flags, app
import cv2
import csv
import time
import numpy as np
import tensorflow as tf
from sc2_env import Sc2EnvWrapper
from agent import Agent
from pysc2.lib import actions
FLAGS = flags.FLAGS

flags.DEFINE_integer('num_episodes', 1000, 'Number of episodes')
flags.DEFINE_integer('batch_size', 10, 'Batch size')
flags.DEFINE_string('data_out_file', None, 'The output data file')

def run_game(env: Sc2EnvWrapper, agent: Agent):
  is_done = False

  A = []
  R = []
  O = []

  obs = env.reset()
  select_army = actions.FunctionCall(7, [[1]])
  obs = env.step([select_army])
  obs = obs[0]

  while not is_done:
    s = obs.observation['feature_screen']

    O.append(s)

    a = agent.forward(s)

    chosen_screen_pos_idx = np.random.choice(np.arange(env.input_size**2), p=a)
    A.append(agent.encode_action(chosen_screen_pos_idx))

    chosen_screen_pos = [chosen_screen_pos_idx // env.input_size, chosen_screen_pos_idx % env.input_size]

    args = [[0], [chosen_screen_pos[1], chosen_screen_pos[0]]]
    a_f = actions.FunctionCall(12, args)

    obs = env.step([a_f])
    obs = obs[0]

    R.append(obs.reward)

    if obs.last():
      is_done = True

  return O, A, agent.discount_rewards(R), R

def _main(unused_args):
  with tf.Session() as sess:
    env = Sc2EnvWrapper()
    agent = Agent(sess)
    agent.build_model()

    if FLAGS.data_out_file:
        with open(FLAGS.data_out_file, 'w') as f:
          writer = csv.writer(f)
          writer.writerow(['n', 'sum_rewards', 'average_rewards', 'policy_loss', 'entropy_loss', 'batch_time'])

    for n in range(FLAGS.num_episodes):
      O = []
      A = []
      R = []
      rewards = []

      now = time.time()
      for _ in range(FLAGS.batch_size):
        o, a, ret, r = run_game(env, agent)

        O.extend(o)
        A.extend(a)
        R.extend(ret)
        rewards.append(r)

      policy_loss, entropy_loss = agent.train(O, A, R)

      sum_rewards = np.average([np.sum(x) for x in rewards])
      average_rewards = np.average(rewards)
      batch_time = time.time() - now

      if FLAGS.data_out_file:
        with open(FLAGS.data_out_file, 'a') as f:
          writer = csv.writer(f)
          writer.writerow([n, sum_rewards, average_rewards, policy_loss, entropy_loss, batch_time])

      print('#{idx} - R: {sumr} / {avgr}'.format(idx=n, sumr=sum_rewards, avgr=average_rewards))

if __name__ == '__main__':
  app.run(_main)