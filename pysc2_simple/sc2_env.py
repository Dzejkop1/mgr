from absl import flags, app
import pysc2
from pysc2.agents.base_agent import BaseAgent
from pysc2.env import sc2_env, run_loop, available_actions_printer
from pysc2.env.sc2_env import Race, Difficulty, Bot, Agent, maps

FLAGS = flags.FLAGS

flags.DEFINE_string('map', 'MoveToBeacon', 'Name of map to use')
flags.DEFINE_integer('feature_screen_size', 32, 'Resolution for screen feature layers')
flags.DEFINE_integer('feature_minimap_size', 32, 'Resolution for minimap feature layers')
flags.DEFINE_integer('rgb_screen_size', None, 'Resolution for rendered screen')
flags.DEFINE_integer('rgb_minimap_size', None, 'Resolution for rendered minimap')
flags.DEFINE_integer('game_step_mul', 16, 'How many games steps per agent step')
flags.DEFINE_enum('action_space', None, sc2_env.ActionSpace.__members__, 'Which action space to use') # pylint: disable=protected-access
flags.DEFINE_bool('use_feature_units', False, 'Whether to include feature units')
flags.DEFINE_bool('visualize', False, 'Whether to visualize the game')

class Sc2EnvWrapper:
  def __init__(self):
    self._env = sc2_env.SC2Env(
      map_name=FLAGS.map,
      step_mul=FLAGS.game_step_mul,
      agent_interface_format=sc2_env.parse_agent_interface_format(
        feature_screen=FLAGS.feature_screen_size,
        feature_minimap=FLAGS.feature_minimap_size,
        rgb_screen=FLAGS.rgb_screen_size,
        rgb_minimap=FLAGS.rgb_minimap_size,
        action_space=FLAGS.action_space,
        use_feature_units=FLAGS.use_feature_units
      ),
      visualize=FLAGS.visualize,
    )
    # print(self._env.observation_spec())
    # self._env = available_actions_printer.AvailableActionsPrinter(self._env)

  def reset(self):
    return self._env.reset()

  def step(self, fcall):
    return self._env.step(fcall)

  @property
  def input_size(self):
    return FLAGS.feature_screen_size