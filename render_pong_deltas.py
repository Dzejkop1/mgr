import gym
import numpy as np
import cv2

env = gym.make('Pong-v0')

obs = env.reset()
is_done = False

while not is_done:
    next_obs, _, is_done, _ = env.step(env.action_space.sample())
    
    delta = next_obs - obs
    obs = next_obs
    
    print('delta: ', delta)
    print('obs: ', obs)
    delta = (delta * 0.5) + (obs * 0.5)
    # delta = np.astype(delta, dtype=int)
    delta = delta.astype(np.uint8)

    print('new_delta: ', delta)
    # delta = cv2.resize(delta, (64, 64))
    # delta = cv2.resize(delta, (256, 256), interpolation = cv2.INTER_NEAREST)
    cv2.imshow('obs', delta)
    cv2.waitKey(100)

