#!/usr/bin/env python3
from matplotlib import pyplot as plt
import csv
import argparse
import numpy as np

def read_csv(f):
    with open(f, 'r') as csv_file:
        reader = csv.reader(csv_file)

        data = [r for r in reader]

        return data

def smooth(y, box_pts):
    box = np.ones(box_pts)/box_pts
    y_smooth = np.convolve(y, box, mode='same')
    return y_smooth

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('sources', nargs='+', help='Files to load data from')
    parser.add_argument('--labels', nargs='*', type=str, help='Labels')
    parser.add_argument('--title', default=None, help='Title of the plot')
    parser.add_argument('--out', help='Target filename. Make sure to include .png or .jpg')
    parser.add_argument('--xlabel', help='Label of the x axis')
    parser.add_argument('--ylabel', help='Label of the y axis')
    parser.add_argument('--xheader', help='Label of the x axis header')
    parser.add_argument('--yheader', help='Label of the y axis header')
    parser.add_argument('-l', '--landscape', help='Render in landscape A4 orientation', action='store_true')
    parser.add_argument('--dpi', default=240, type=int, help='Definition of the plot')
    parser.add_argument('--smoothing', type=int)

    args = parser.parse_args()

    print(args)

    data = [read_csv(f) for f in args.sources]
    if args.landscape:
        plt.figure(figsize=(12, 6))
    for d in data:
        if args.xheader and args.yheader:
            # d = list(zip(*d))
            headers = d[0]

            d = d[1:]

            d = list(zip(*d))

            xindex = headers.index(args.xheader)
            yindex = headers.index(args.yheader)

            x = d[xindex]
            y = d[yindex]
        else:
            x, y = zip(*d)

        x = [float(v) for v in x]
        y = [float(v) for v in y]

        print(x)
        if args.smoothing:
            y = smooth(y, args.smoothing)

        plt.plot(x, y)

    if args.title:
        plt.title(args.title)

    if args.labels:
        plt.legend(args.labels)

    if args.xlabel:
        plt.xlabel(args.xlabel)

    if args.ylabel:
        plt.ylabel(args.ylabel)

    plt.savefig(args.out, papertype='a4', orientation='landscape', dpi=args.dpi)

main()
