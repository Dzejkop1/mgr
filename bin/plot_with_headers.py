#!/usr/bin/env python3
from matplotlib import pyplot as plt
import csv
import argparse
import numpy as np

def read_csv(f):
    with open(f, 'r') as csv_file:
        reader = csv.reader(csv_file)

        return [row for row in reader]

def smooth(x, y, box_pts):
    box = np.ones(box_pts) / box_pts
    y_smooth = np.convolve(y, box, mode='same')
    y_smooth[-1] = y[-1]
    return x[box_pts:-box_pts], y_smooth[box_pts:-box_pts]

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('source', type=str, help='Files to load data from')
    parser.add_argument('--title', default=None, help='Title of the plot')
    parser.add_argument('--out', help='Target filename. Make sure to include .png or .jpg or .esp')
    parser.add_argument('--xlabel', help='Label of the x axis')
    parser.add_argument('--ylabel', help='Label of the y axis')
    parser.add_argument('-l', '--landscape', help='Render in landscape A4 orientation', action='store_true')
    parser.add_argument('--dpi', default=240, type=int, help='Definition of the plot')
    parser.add_argument('--headers', nargs='*', help='A list of headers to plot on the graph. If not specified will plot all')
    parser.add_argument('-s', '--smoothing', type=int, default=0, help='Smoothing factor. If 0 no smoothing.')
    parser.add_argument('--step', type=int, default=1, help='Skip step')

    args = parser.parse_args()

    data = read_csv(args.source)

    headers = data[0]
    data = data[1:]
    data = list(zip(*data))

    data = [np.array(d, dtype=np.float32) for d in data]

    if args.headers:
      indices = []
      for h in args.headers:
        indices.append(headers.index(h))

      headers = args.headers
      data = [data[i] for i in indices]

    # plt.legend(headers)

    if args.landscape:
        plt.figure(figsize=(12, 6))

    x = data[0]
    data = data[1:]

    if args.step:
      x = [x[i] for i in range(0, len(x), args.step)]
      data = [[d[i] for i in range(0, len(d), args.step)] for d in data]

    print(x)

    for d in data:
        # print(d)
        y = d
        # d = list(zip(*d))
        # print(d)
        if args.smoothing != 0:
          nx, y = smooth(x, y, args.smoothing)
        else:
          nx = x

        plt.plot(nx, y)

    if args.title:
        plt.title(args.title)

    if args.xlabel:
        plt.xlabel(args.xlabel)

    if args.ylabel:
        plt.ylabel(args.ylabel)

    plt.savefig(args.out, papertype='a4', orientation='landscape', dpi=args.dpi)

main()
