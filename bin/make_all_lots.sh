PATH_TO_PLOT=bin/plot.py

python $PATH_TO_PLOT temp/exp_beta/cp*.csv --xheader n --yheader average_episode_reward_sum --out exp_beta.png --labels 0.0 0.1 0.5 0.9 1.0 5.0

python $PATH_TO_PLOT temp/exp_gamma/cp*.csv --xheader n --yheader average_episode_reward_sum --out exp_gamma.png --labels 0.0 0.5 0.9 0.99 0.999 1.0

python $PATH_TO_PLOT temp/exp_lr/cp*.csv --xheader n --yheader average_episode_reward_sum --out exp_lr.png --labels 0.1 0.01 0.005 0.001 0.0005 0.0001

python $PATH_TO_PLOT temp/exp_net/cp{1,2,3,4}.csv --xheader n --yheader average_episode_reward_sum --out exp_one.png --labels n4 n8 n16 n32
python $PATH_TO_PLOT temp/exp_net/cp{5,6,7}.csv --xheader n --yheader average_episode_reward_sum --out exp_n32.png --labels n32_16r n32_16s n32_16
python $PATH_TO_PLOT temp/exp_net/cp{8,9,10}.csv --xheader n --yheader average_episode_reward_sum --out exp_n16.png --labels n16_8r n16_8s n16_8
