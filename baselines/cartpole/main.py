import gym
import time

def react(obs):
    tip_vel = obs[3]
    tip_angle = obs[2]

    if tip_vel > 0.1:
        return 1
    elif tip_vel < -0.1:
        return 0

    if tip_angle > 0:
        return 1
    else:
        return 0

def main():
    env = gym.make('CartPole-v1')
    
    is_done = False
    obs = env.reset()
    reward = 0

    while not is_done:
        action = react(obs)
        obs, r, is_done, _ = env.step(action)
        env.render()
        reward += r
        time.sleep(0.01)

    return reward

r = main()
input('DONE %d' % r)
