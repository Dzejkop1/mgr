import gym
import time

env = gym.make('Acrobot-v1')

obs = env.reset()
is_done = False

while not is_done:
    action = env.action_space.sample()
    obs, _, is_done, _ = env.step(action)

    env.render()
    print ('Action: ', action)
    print ('Obs: ', obs)
    #time.sleep(1)
