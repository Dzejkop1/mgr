import tensorflow as tf
import tensorflow.contrib.layers as layers
import tensorflow.keras.layers as K

def build_net(obs):
    flat = K.Flatten()(obs)
    fc1 = K.Dense(256, activation='relu')(flat)

    return fc1