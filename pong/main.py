import tensorflow as tf
import tensorflow.contrib.layers as layers
import tensorflow.keras.layers as K
import numpy as np
import gym
import cv2
import csv
import importlib
from absl import flags, app
from random import random
from multiprocessing.pool import ThreadPool as Pool
from multiprocessing import Lock

def softmax(x):
  return np.exp(x) / np.sum(np.exp(x), axis=0)

def discount_rewards(rewards, gamma=0.99):
  r = np.array([gamma**i * rewards[i]
                for i in range(len(rewards))])
  r = r[::-1].cumsum()[::-1]
  return r

class Agent:
    def __init__(self, sess: tf.Session):
        self.sess = sess

    def build_model(self, network_builder, input_shape, output_shape, ):
        self.learning_rate = tf.placeholder(tf.float32, (), name='learning_rate')
        self.beta = tf.placeholder(tf.float32, (), name='beta')

        self.observation = tf.placeholder(tf.float32, [None] + input_shape, name='observation')

        self.final = network_builder(self.observation)

        self.action_output = K.Dense(output_shape, activation='softmax')(self.final)

        self.selected_action = tf.placeholder(tf.float32, [None, output_shape], name='selected_action')
        self.discounted_reward = tf.placeholder(tf.float32, [None], name='discounted_rewards')
        self.selected_action_log_prob = tf.log(tf.reduce_sum(self.selected_action * self.action_output, axis=1))
        self.average_discounted_return = tf.reduce_mean(self.discounted_reward)

        # advantage_estimate = tf.stop_gradient(self.discounted_reward - self.value)
        advantage_estimate = tf.stop_gradient(self.discounted_reward - tf.reduce_mean(self.discounted_reward))

        policy_loss = -tf.reduce_sum(self.selected_action_log_prob * advantage_estimate)

        self.entropy_loss = tf.reduce_sum(self.action_output * tf.log(tf.clip_by_value(self.action_output, 1e-10, 1)))

        tf.summary.scalar('entropy_loss', self.entropy_loss)
        tf.summary.scalar('policy_loss', policy_loss)

        loss = policy_loss + (self.beta * self.entropy_loss)

        tf.summary.scalar('loss', loss)

        tf.summary.scalar('average_discounted_return', self.average_discounted_return)
        self.merged = tf.summary.merge_all()

        self.train_writer = tf.summary.FileWriter('./log/pong', graph=self.sess.graph)
        self.run_metadata = tf.RunMetadata()

        opt = tf.train.RMSPropOptimizer(self.learning_rate)
        self.train_op = opt.minimize(loss)

    def forward(self, delta):
        a = self.sess.run(self.action_output, feed_dict={
            self.observation: [delta],
        })

        return a.ravel()


    def train(self, deltas, rewards, actions, lr=1e-2, beta=0.5):
        run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
        summary, _ = self.sess.run([self.merged, self.train_op], feed_dict={
            self.observation: deltas,
            self.selected_action: actions,
            self.discounted_reward: rewards,
            self.beta: beta,
            self.learning_rate: lr,
        },
        run_metadata=self.run_metadata,
        options=run_options)

        self.train_writer.add_summary(summary)
        # self.train_writer.add_run_metadata(self.run_metadata, 'xD')


FLAGS = flags.FLAGS

flags.DEFINE_bool('visualize', False, 'Visualize the environment')
flags.DEFINE_integer('visualize_delay', 1, 'Time between visualization frames. If 0 will wait for user key')
flags.DEFINE_string('env', 'Pong-v0', 'Gym environment')
flags.DEFINE_integer('threads', 4, 'How many threads to use')

flags.DEFINE_string('net', 'nets.basic', 'Neural net to use')
flags.DEFINE_integer('input_size', 64, 'If should resize the input to a box')
flags.DEFINE_string('exp_out', None, 'Where to store experiment data')

# Hyperparameters
flags.DEFINE_integer('num_episodes', 5000, 'Number of episodes')
flags.DEFINE_integer('batch_size', 100, 'Batch size')
flags.DEFINE_float('learning_rate', 1e-3, 'Learning rate')
flags.DEFINE_float('gamma', 1 - 1e-6, 'Discount gamma')
flags.DEFINE_float('beta', 1e-8, 'Entropy loss coefficient')
flags.DEFINE_float('delta_factor', 0.5, 'How significant is the delta in an observation')

env_init_mutex = Lock()

shared_env_mutex = Lock()

def run_batch(args):
    # while True:
    #     try:
    return run_batch_inner(args)
        # except:
        #     print('Something went wrong, retrying... ')

def make_delta(prev_obs, curr_obs):
    delta = curr_obs - prev_obs

    obs = (delta * FLAGS.delta_factor) + (curr_obs * (1.0 - FLAGS.delta_factor))

    return obs

def preprocess_obs(obs):
    img = cv2.resize(obs, (FLAGS.input_size, FLAGS.input_size))
    img = img.astype(np.float32) / 255.0

    return img

def run_batch_inner(args):
    agent, env_creator, i = args

    with env_init_mutex:
        env = env_creator()

    if FLAGS.visualize:
        shared_env_mutex.acquire()

    obs = env.reset()
    obs = preprocess_obs(obs)

    is_done = False

    delta = make_delta(np.zeros_like(obs), obs)

    episode_deltas = []
    episode_new_deltas = []
    episode_rewards = []
    episode_actions = []

    while not is_done:
        episode_deltas.append(delta)

        action_probs = agent.forward(delta)

        action = np.random.choice(np.arange(env.action_space.n), p=action_probs)

        new_obs, reward, is_done, _ = env.step(action)
        new_obs = preprocess_obs(new_obs)

        delta = make_delta(obs, new_obs)

        episode_new_deltas.append(delta)
        obs = new_obs

        if FLAGS.visualize:
            env.render()
            # cv2.imshow('obs', cv2.resize(delta, (160, 210)), interpolation=cv2.INTER_NEAREST)
            # cv2.imshow('obs', cv2.resize(delta, (160, 210), interpolation=cv2.INTER_NEAREST))
            # cv2.waitKey(FLAGS.visualize_delay)

        action_one_hot = np.zeros(env.action_space.n)
        action_one_hot[action] = 1.0

        episode_actions.append(action_one_hot)
        episode_rewards.append(reward)

    discount_episode_rewards = discount_rewards(episode_rewards, FLAGS.gamma)

    if FLAGS.visualize:
        shared_env_mutex.release()

    return episode_rewards, discount_episode_rewards, episode_actions, episode_deltas

def _main(unused_args):
    with tf.Session() as sess:
        i = 0
        n = 0
        if FLAGS.visualize:
            env = gym.make(FLAGS.env)
            env_creator = lambda : env
        else:
            env_creator = lambda : gym.make(FLAGS.env)

        network_module, network_name = FLAGS.net.rsplit('.', 1)
        network_builder = getattr(importlib.import_module(network_module), network_name)

        agent = Agent(sess)

        tmp_env = env_creator()

        if isinstance(tmp_env.action_space, gym.spaces.Discrete):
            agent_action_space = tmp_env.action_space.n
        elif isinstance(tmp_env.action_space, gym.spaces.Box):
            agent_action_space = list(tmp_env.action_space.shape)
        else:
            raise Exception('Invalid action space')

        if isinstance(tmp_env.observation_space, gym.spaces.Discrete):
            agent_obs_space = tmp_env.observation_space.n
        elif isinstance(tmp_env.observation_space, gym.spaces.Box):
            agent_obs_space = list(tmp_env.observation_space.shape)
        else:
            raise Exception('Invalid observation space')

        tmp_env = None

        agent.build_model(network_builder, [FLAGS.input_size, FLAGS.input_size, 3], agent_action_space)

        sess.run(tf.global_variables_initializer())

        pool = Pool(FLAGS.threads)

        exp_data = []

        while True:
            max_bound = min(n + FLAGS.batch_size, FLAGS.num_episodes)
            batch_runs = [(agent, env_creator, i) for i in range(n, max_bound)]

            r, batch_rewards, batch_actions, batch_deltas = list(zip(*pool.map(run_batch, batch_runs)))

            n += FLAGS.batch_size
            i += 1

            r = [np.sum(x) for x in r]
            r = np.average(r)

            batch_rewards = np.concatenate(batch_rewards)
            batch_actions = np.concatenate(batch_actions)
            batch_deltas = np.concatenate(batch_deltas)

            print (('#%d - R: ' % n), r)

            exp_data.append([i, r])
            agent.train(
                batch_deltas,
                batch_rewards,
                batch_actions,
                FLAGS.learning_rate,
                FLAGS.beta)

            if n >= FLAGS.num_episodes:
                break

        if FLAGS.exp_out != None:
            with open(FLAGS.exp_out, 'w') as f:
                writer = csv.writer(f, delimiter=',')

                for r in exp_data:
                    writer.writerow(r)

if __name__ == '__main__':
    app.run(_main)
