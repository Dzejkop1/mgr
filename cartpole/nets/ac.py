import tensorflow.keras.layers as K
import tensorflow as tf

def a32_16r_v8(i):
    a = K.Dense(32)(i)
    a = K.Dense(16, activation='relu')(a)

    c = K.Dense(8)(i)

    return a, c