import tensorflow.keras.layers as K
import tensorflow as tf

def make(i, conv_layers=[(1, 16, 4)], layers=[(256, 'relu')]):
    n, k, s = conv_layers[0]

    conv = K.Conv2D(n, k, s)(i)

    for n, k, s in conv_layers[1:]:
        print('Conv layers: ', conv.shape)
        conv = K.Conv2D(n, k, s)(conv)

    print('Conv layers: ', conv.shape)

    fc = K.Flatten()(conv)

    for n, a in layers:
        fc = K.Dense(n, activation=a)(fc)

    return fc

def c_3_16_4_c_10_8_8_n256s_128_64r(i):
    return make(i, [
        (1, 8, 4),
        (1, 4, 2),
    ],
    [
        (64, None),
        (32, None),
    ])

def c1_16_8_c10_4_1_n256s_128s_64r(i):
    return make(i, [
        (1, 16, 8),
        (1, 4, 1),
    ], [
        (256, 'sigmoid'),
        (128, 'sigmoid'),
        (64, 'relu'),
    ])

def c1_16_8_n256_128_64r(i):
    return make(i, [
        (1, 16, 8),
    ], [
        (256, None),
        (128, None),
        (64, 'sigmoid'),
        (32, 'relu'),
    ])

def c5_8_2_c1_4_2_n512_256_128(i):
    return make(i, [
        (5, 8, 2),
        (1, 4, 2),
    ], [
        (512, 'sigmoid'),
        (256, None),
        (128, 'relu'),
    ])

# def convolutional(i):
#     conv1 = K.Conv2D(1, 16, 4)(i)
#     conv2 = K.Conv2D(10, 4, 1)(conv1)

#     # prev_fc1 = tf.placeholder(tf.float32, [None, 256], name='prev')
#     fc1 = K.Dense(256, activation='sigmoid')(K.Flatten()(conv2))

#     # combined = tf.concat([fc1, prev_fc1], 1)

#     # fc1 = layers.fully_connected(observation, num_outputs=128, activation_fn=tf.nn.softmax)
#     fc2 = K.Dense(128, activation='sigmoid')(fc1)
#     final = K.Dense(64, activation='relu')(fc2)

#     return final