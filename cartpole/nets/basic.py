import tensorflow.keras.layers as K
import tensorflow as tf

def make(i, layers=[]):
    fc = None
    for n, a in layers:
        if fc == None:
            fc = K.Dense(n, activation=a)(i)
        else:
            fc = K.Dense(n, activation=a)(fc)

    return fc

def n4(i):
    return make(i, [(4, None)])

def n8(i):
    return make(i, [(8, None)])

def n16(i):
    return make(i, [(8, None)])

def n32(i):
    return make(i, [(8, None)])

def n32_16r(i):
    return make(i, [
        (32, None),
        (16, 'relu'),
    ])

def n32_16(i):
    return make(i, [
        (32, None),
        (16, None),
    ])

def n32r_16r(i):
    return make(i, [
        (32, 'relu'),
        (16, 'relu'),
    ])

def n32s_16s(i):
    return make(i, [
        (32, 'sigmoid'),
        (16, 'sigmoid'),
    ])

def n128s_64_32_16r(i):
    return make(i, [
        (128, 'sigmoid'),
        (64, None),
        (32, None),
        (16, 'relu')
    ])

def n128s_64_32_16(i):
    return make(i, [
        (128, 'sigmoid'),
        (64, None),
        (32, None),
        (16, None)
    ])

def n256_128(i):
    return make(i, [
        (256, 'sigmoid'),
        (128, None),
    ])


def n256s_128(i):
    return make(i, [
        (256, 'sigmoid'),
        (128, None),
    ])

def n256s_128s(i):
    return make(i, [
        (256, 'sigmoid'),
        (128, 'sigmoid'),
    ])

def n256s_128s_32r(i):
    return make(i, [
        (256, 'sigmoid'),
        (128, 'sigmoid'),
        (32, 'relu'),
    ])

def n1024s_512_256_128_64r(i):
    return make(i, [
        (256, None),
        (32, 'relu')
    ])
