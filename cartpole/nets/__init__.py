import tensorflow.keras.layers as K
import tensorflow as tf

def convolutional(input_tensor):
    conv1 = K.Conv2D(1, 16, 4)(input_tensor)
    conv2 = K.Conv2D(10, 4, 1)(conv1)

    # prev_fc1 = tf.placeholder(tf.float32, [None, 256], name='prev')
    fc1 = K.Dense(256, activation='sigmoid')(K.Flatten()(conv2))

    # combined = tf.concat([fc1, prev_fc1], 1)

    # fc1 = layers.fully_connected(observation, num_outputs=128, activation_fn=tf.nn.softmax)
    fc2 = K.Dense(128, activation='sigmoid')(fc1)
    final = K.Dense(64, activation='relu')(fc2)

    return final

def conv_2d_ac_ssr(input_tensor):
    conv1 = K.Conv2D(1, 16, 4)(input_tensor)
    conv2 = K.Conv2D(10, 4, 1)(conv1)

    fc1 = K.Dense(256, activation='sigmoid')(K.Flatten()(conv2))
    fc2 = K.Dense(128, activation='sigmoid')(fc1)
    final = K.Dense(64, activation='relu')(fc2)

    value_fc1 = K.Dense(128)(fc1)
    value = K.Dense(1)(value_fc1)

    return final, value

def from_string(net_string):
    """
    Converts a string into a neural network. Only supports single input-output

    The network string looks something like this
    n32_16r
    n128s_20
    c(2, 16, 8)_c(1, 8, 4)_n1024s_n32r

    Each layer is separated with an underscore
    a c specifies a convolutional layer
    an n specifies a dense layer
    For a c network 3 arguments must be provided like so c(num_outputs, kernel_size, stride)
    """
    def network_builder(input_tensor):
        layers = net_string.split('_')
        previous_was_convolutional = None
        curr_layer = None

        for l in layers:
            if l[0] == 'c':
                if previous_was_convolutional != None and not previous_was_convolutional:
                    raise Exception('Invalid format')

                previous_was_convolutional = True
                n, k, s = (int(a) for a in l[2:-1].split(','))

                if curr_layer != None:
                    curr_layer = K.Conv2D(n, k, s)(curr_layer)
                else:
                    curr_layer = K.Conv2D(n, k, s)(input_tensor)
            if l[0] == 'n':
                if previous_was_convolutional:
                    curr_layer = K.Flatten()(curr_layer)

                previous_was_convolutional = False

                layer_type = l[-1]
                if layer_type in ['r', 's', 'S']:
                    n = int(l[1:-1])
                else:
                    layer_type = None
                    n = int(l[1:])

                activations = {
                    'r': 'relu',
                    's': 'sigmoid',
                    'S': 'softmax',
                    None: None
                }

                if curr_layer != None:
                    curr_layer = K.Dense(n, activation=activations[layer_type])(curr_layer)
                else:
                    curr_layer = K.Dense(n, activation=activations[layer_type])(input_tensor)

        return curr_layer

    return network_builder