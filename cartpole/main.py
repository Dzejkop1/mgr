import tensorflow as tf
import tensorflow.contrib.layers as layers
import tensorflow.keras.layers as K
import numpy as np
import gym
import cv2
import csv
import importlib
from absl import flags, app
from random import random
from multiprocessing.pool import ThreadPool as Pool
from multiprocessing import Lock

def softmax(x):
  return np.exp(x) / np.sum(np.exp(x), axis=0)

def discount_rewards(rewards, gamma=0.99):
  r = np.array([gamma**i * rewards[i]
                for i in range(len(rewards))])
  r = r[::-1].cumsum()[::-1]
  return r

class Agent:
    def __init__(self, sess: tf.Session):
        self.sess = sess

    def build_model(self, network_builder, input_shape, output_shape, use_value=False):
        self.is_using_value = use_value

        self.alpha = tf.placeholder(tf.float32, (), name='alpha')
        self.learning_rate = tf.placeholder(tf.float32, (), name='learning_rate')
        self.beta = tf.placeholder(tf.float32, (), name='beta')

        self.observation = tf.placeholder(tf.float32, [None] + input_shape, name='observation')

        if use_value:
            self.final, value = network_builder(self.observation)
            value = K.Dense(1)(value)
            self.value = tf.reshape(value, [-1,])
        else:
            self.final = network_builder(self.observation)

        self.action_output = K.Dense(output_shape, activation='softmax')(self.final)

        self.selected_action = tf.placeholder(tf.float32, [None, output_shape], name='selected_action')
        self.discounted_reward = tf.placeholder(tf.float32, [None], name='discounted_rewards')

        self.values = tf.placeholder(tf.float32, [None], name='values')

        self.selected_action_log_prob = tf.log(tf.reduce_sum(self.selected_action * self.action_output, axis=1))

        self.average_discounted_return = tf.reduce_mean(self.discounted_reward)
        if use_value:
            # advantage_estimate = tf.stop_gradient(self.discounted_reward - self.value)
            advantage_estimate = self.discounted_reward - self.value
            self.value_loss = tf.reduce_mean(tf.square(self.value - self.values))

            tf.summary.scalar('value_loss', self.value_loss)
        else:
            advantage_estimate = tf.stop_gradient(self.discounted_reward - self.average_discounted_return)

        self.policy_loss = -tf.reduce_sum(self.selected_action_log_prob * advantage_estimate)
        self.entropy_loss = tf.reduce_sum(self.action_output * tf.log(tf.clip_by_value(self.action_output, 1e-10, 1)))

        tf.summary.scalar('entropy_loss', self.entropy_loss)
        tf.summary.scalar('policy_loss', self.policy_loss)

        if use_value:
            loss = self.policy_loss + (self.beta * self.entropy_loss) + (self.alpha * self.value_loss)
        else:
            loss = self.policy_loss + (self.beta * self.entropy_loss)

        tf.summary.scalar('average_discounted_return', self.average_discounted_return)
        self.merged = tf.summary.merge_all()

        self.train_writer = tf.summary.FileWriter('./log/xD_', graph=self.sess.graph)
        self.run_metadata = tf.RunMetadata()

        opt = tf.train.RMSPropOptimizer(self.learning_rate)
        self.train_op = opt.minimize(loss)

    def forward(self, obs):
        if self.is_using_value:
            a, v = self.sess.run([self.action_output, self.value], feed_dict={
                self.observation: [obs],
            })

            return a.ravel(), v.ravel()
        else:
            a = self.sess.run(self.action_output, feed_dict={
                self.observation: [obs]
            })

            return a.ravel(), 0


    def train(self, obs, rewards, actions, values, lr=1e-2, alpha=0.5, beta=0.5):
        if self.is_using_value:
            self.sess.run(self.train_op, feed_dict={
                self.observation: obs,
                self.selected_action: actions,
                self.discounted_reward: rewards,
                self.values: values,
                self.beta: beta,
                self.alpha: alpha,
                self.learning_rate: lr,
            })

            # self.train_writer.add_summary(summary)
            # self.train_writer.add_run_metadata(self.run_metadata, 'xD')

        else:
            p_loss, e_loss, _ = self.sess.run([self.policy_loss, self.entropy_loss, self.train_op], feed_dict={
                self.observation: obs,
                self.selected_action: actions,
                self.discounted_reward: rewards,
                self.beta: beta,
                self.alpha: alpha,
                self.learning_rate: lr,
            })

            return p_loss.item(), e_loss.item()

FLAGS = flags.FLAGS

flags.DEFINE_bool('visualize', False, 'Visualize the environment')
flags.DEFINE_integer('visualize_mod', -1, 'Modulo of batch at which to visualize')
flags.DEFINE_string('env', 'CartPole-v0', 'Gym environment')
flags.DEFINE_integer('threads', 4, 'How many threads to use')

flags.DEFINE_string('net', 'nets.basic', 'Neural net to use')
flags.DEFINE_bool('use_value', False, 'Use the value baseline and value loss')
# flags.DEFINE_list('')
flags.DEFINE_integer('resize_obs_to_box', None, 'If should resize the input to a box')
flags.DEFINE_string('exp_out', None, 'Where to store experiment data')

# Hyperparameters
flags.DEFINE_integer('training_runs', 1, 'Number of training runs in the experiment')
flags.DEFINE_float('noise_magnitude', 0.1, 'Magnitude of noise')
flags.DEFINE_integer('num_episodes', 5000, 'Number of episodes')
flags.DEFINE_integer('batch_size', 100, 'Batch size')
flags.DEFINE_float('learning_rate', 1e-2, 'Learning rate')
flags.DEFINE_float('gamma', 0.99, 'Discount gamma')
flags.DEFINE_float('alpha', 1.0, 'Value loss coefficient')
flags.DEFINE_float('beta', 0.2, 'Entropy loss coefficient')
flags.DEFINE_bool('reuse_env', False, 'Whether to reuse environment, or not.')

model_mutex = Lock()
env_init_mutex = Lock()

shared_env_mutex = Lock()

def run_batch(args):
    # while True:
    #     try:
    return run_batch_inner(args)
        # except:
        #     print('Something went wrong, retrying... ')

def run_batch_inner(args):
    agent, env_creator, i = args

    # print('#' + str(i))
    with env_init_mutex:
        env = env_creator()

    if FLAGS.reuse_env:
        shared_env_mutex.acquire()

    if FLAGS.resize_obs_to_box != None:
        preprocess_input = lambda obs: cv2.resize(obs, (FLAGS.resize_obs_to_box, FLAGS.resize_obs_to_box))
        obs = preprocess_input(env.reset())
    else:
        obs = env.reset()

    is_done = False

    episode_rewards = []
    episode_actions = []
    episode_observations = []
    episode_values = []

    while not is_done:
        if FLAGS.visualize:
            env.render()

        episode_observations.append(obs)

        # with model_mutex:
        action_probs, value = agent.forward(obs)

        # noise = np.random.randn(*action_probs.shape)
        # action_probs = softmax(action_probs + (FLAGS.noise_magnitude * noise))

        # print('Obs: ', obs)
        # print('Action probs: ', action_probs)
        action = np.random.choice(np.arange(env.action_space.n), p=action_probs)

        # print('Picked action is: ', action)
        obs, reward, is_done, _ = env.step(action)

        if FLAGS.resize_obs_to_box != None:
            obs = preprocess_input(obs)

        if is_done:
            episode_values.append(0)
        else:
            episode_values.append(value)

        action_one_hot = np.zeros(env.action_space.n)
        action_one_hot[action] = 1.0

        episode_actions.append(action_one_hot)
        episode_rewards.append(reward)

    discount_episode_rewards = discount_rewards(episode_rewards, FLAGS.gamma)

    if FLAGS.reuse_env:
        shared_env_mutex.release()

    return episode_rewards, discount_episode_rewards, episode_actions, episode_observations, discount_episode_rewards

def training_run():
    with tf.Session() as sess:
        i = 0
        n = 0
        if FLAGS.reuse_env:
            env = gym.make(FLAGS.env)
            env_creator = lambda : env
        else:
            env_creator = lambda : gym.make(FLAGS.env)


        if '.' in FLAGS.net:
            network_module, network_name = FLAGS.net.rsplit('.', 1)
            network_builder = getattr(importlib.import_module(network_module), network_name)
        else:
            import nets
            network_builder = nets.from_string(FLAGS.net)

        agent = Agent(sess)

        tmp_env = env_creator()

        if isinstance(tmp_env.action_space, gym.spaces.Discrete):
            agent_action_space = tmp_env.action_space.n
        elif isinstance(tmp_env.action_space, gym.spaces.Box):
            agent_action_space = list(tmp_env.action_space.shape)
        else:
            raise Exception('Invalid action space')

        if isinstance(tmp_env.observation_space, gym.spaces.Discrete):
            agent_obs_space = tmp_env.observation_space.n
        elif isinstance(tmp_env.observation_space, gym.spaces.Box):
            agent_obs_space = list(tmp_env.observation_space.shape)
        else:
            raise Exception('Invalid observation space')

        if FLAGS.resize_obs_to_box != None:
            agent.build_model(network_builder, [FLAGS.resize_obs_to_box, FLAGS.resize_obs_to_box, 3], agent_action_space, use_value=FLAGS.use_value)
        else:
            agent.build_model(network_builder, agent_obs_space, agent_action_space, use_value=FLAGS.use_value)

        sess.run(tf.global_variables_initializer())

        pool = Pool(FLAGS.threads)

        exp_data = []
        while True:
            max_bound = min(n + FLAGS.batch_size, FLAGS.num_episodes)
            # print ('MAX_BOUND:', max_bound)
            batch_runs = [(agent, env_creator, i) for i in range(n, max_bound)]

            r, batch_rewards, batch_actions, batch_observations, batch_values = list(zip(*pool.map(run_batch, batch_runs)))
            # batch_rewards, batch_actions, batch_observations, batch_values = run_batch((agent, env_creator, n))

            n += FLAGS.batch_size
            i += 1

            average_episode_reward_sum = [np.sum(x) for x in r]
            average_episode_reward_sum = np.average(average_episode_reward_sum)

            average_episode_reward = [np.average(x) for x in r]
            average_episode_reward = np.average(average_episode_reward)

            batch_rewards = np.concatenate(batch_rewards)
            batch_actions = np.concatenate(batch_actions)
            batch_observations = np.concatenate(batch_observations)
            batch_values = np.concatenate(batch_values)

            # print (('#%d - R: ' % n), average_episode_reward_sum)

            # print ('Sum(R): ', np.sum(r))
            # if batch_counter >= FLAGS.batch_size:
            p_loss, e_loss = agent.train(
                batch_observations,
                batch_rewards,
                batch_actions,
                batch_values,
                FLAGS.learning_rate,
                FLAGS.alpha,
                FLAGS.beta)

            exp_data.append([i, average_episode_reward, average_episode_reward_sum, p_loss, e_loss])

            if n >= FLAGS.num_episodes:
                break
    return exp_data

def _main(unused_args):
    header = ['n', 'average_episode_reward', 'average_episode_reward_sum', 'policy_loss', 'entropy_loss']
    exp_data = []
    for i in range(FLAGS.training_runs):
      print('Training run #{}'.format(i))
      exp_data.append(training_run())

    exp_data = np.average(exp_data, axis=0)
    # exp_data = list(zip(*exp_data))

    # exp_data = np.average(exp_data, axis=1)

    if FLAGS.exp_out != None:
        with open(FLAGS.exp_out, 'w') as f:
            writer = csv.writer(f, delimiter=',')

            writer.writerow(header)
            for r in exp_data:
                writer.writerow(r)

    # batch_rewards = []
    # batch_actions = []
    # batch_observations = []

    # batch_counter = 0

if __name__ == '__main__':
    app.run(_main)
