BASE_DIR=exp_net
BATCH_SIZE=10
NUM_EPISODES=1000
TRAINING_RUNS=100

mkdir $BASE_DIR

# CartPole 1
python main.py --training_runs $TRAINING_RUNS --env CartPole-v0 --num_episodes $NUM_EPISODES --threads 4 --batch_size $BATCH_SIZE --alpha 0.0 --beta 0.2 --learning_rate 0.01 --net n4 --exp_out $BASE_DIR/cp1.csv

# CartPole 2
python main.py --training_runs $TRAINING_RUNS --env CartPole-v0 --num_episodes $NUM_EPISODES --threads 4 --batch_size $BATCH_SIZE --alpha 0.0 --beta 0.2 --learning_rate 0.01 --net n8 --exp_out $BASE_DIR/cp2.csv

# CartPole 3
python main.py --training_runs $TRAINING_RUNS --env CartPole-v0 --num_episodes $NUM_EPISODES --threads 4 --batch_size $BATCH_SIZE --alpha 0.0 --beta 0.2 --learning_rate 0.01 --net n16 --exp_out $BASE_DIR/cp3.csv

# CartPole 4
python main.py --training_runs $TRAINING_RUNS --env CartPole-v0 --num_episodes $NUM_EPISODES --threads 4 --batch_size $BATCH_SIZE --alpha 0.0 --beta 0.2 --learning_rate 0.01 --net n32 --exp_out $BASE_DIR/cp4.csv


# CartPole 5
python main.py --training_runs $TRAINING_RUNS --env CartPole-v0 --num_episodes $NUM_EPISODES --threads 4 --batch_size $BATCH_SIZE --alpha 0.0 --beta 0.2 --learning_rate 0.01 --net n32_16r --exp_out $BASE_DIR/cp5.csv

# CartPole 6
python main.py --training_runs $TRAINING_RUNS --env CartPole-v0 --num_episodes $NUM_EPISODES --threads 4 --batch_size $BATCH_SIZE --alpha 0.0 --beta 0.2 --learning_rate 0.01 --net n32_16s --exp_out $BASE_DIR/cp6.csv

# CartPole 7
python main.py --training_runs $TRAINING_RUNS --env CartPole-v0 --num_episodes $NUM_EPISODES --threads 4 --batch_size $BATCH_SIZE --alpha 0.0 --beta 0.2 --learning_rate 0.01 --net n32_16 --exp_out $BASE_DIR/cp7.csv


# CartPole 8
python main.py --training_runs $TRAINING_RUNS --env CartPole-v0 --num_episodes $NUM_EPISODES --threads 4 --batch_size $BATCH_SIZE --alpha 0.0 --beta 0.2 --learning_rate 0.01 --net n16_8r --exp_out $BASE_DIR/cp8.csv

# CartPole 9
python main.py --training_runs $TRAINING_RUNS --env CartPole-v0 --num_episodes $NUM_EPISODES --threads 4 --batch_size $BATCH_SIZE --alpha 0.0 --beta 0.2 --learning_rate 0.01 --net n16_8s --exp_out $BASE_DIR/cp9.csv

# CartPole 10
python main.py --training_runs $TRAINING_RUNS --env CartPole-v0 --num_episodes $NUM_EPISODES --threads 4 --batch_size $BATCH_SIZE --alpha 0.0 --beta 0.2 --learning_rate 0.01 --net n16_8 --exp_out $BASE_DIR/cp10.csv
