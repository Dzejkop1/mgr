MINI_MAP_URL="https://github.com/deepmind/pysc2/releases/download/v1.2/mini_games.zip"
BINARY_URL="http://blzdistsc2-a.akamaihd.net/Linux/SC2.4.7.1.zip"

PASS="http://blzdistsc2-a.akamaihd.net/Linux/SC2.4.7.1.zip"

pushd ~

curl $BINARY_URL -o "SC2.zip"
unzip "SC2.zip"

rm "SC2.zip"

pushd "StarCraftII/Maps"

curl -L $MINI_MAP_URL -o "minimaps.zip"
unzip "minimaps.zip"

rm "minimaps.zip"

popd
popd
