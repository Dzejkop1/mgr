from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import time
import pysc2
from pysc2.agents.base_agent import BaseAgent
from pysc2.env import sc2_env, run_loop
from pysc2.env.sc2_env import Race, Difficulty, Bot, Agent, maps
from absl import flags, app
import tensorflow as tf

from agent import VPGAgent

FLAGS = flags.FLAGS

flags.DEFINE_string('map', 'MoveToBeacon', 'Name of map to use')
flags.DEFINE_string('agent', 'main.MyAgent', 'Agent class to use')
flags.DEFINE_integer('feature_screen_size', 16, 'Resolution for screen feature layers')
flags.DEFINE_integer('feature_minimap_size', 16, 'Resolution for minimap feature layers')
flags.DEFINE_integer('rgb_screen_size', None, 'Resolution for rendered screen')
flags.DEFINE_integer('rgb_minimap_size', None, 'Resolution for rendered minimap')
flags.DEFINE_integer('game_step_mul', 8, 'How many games steps per agent step')
flags.DEFINE_enum('action_space', None, sc2_env.ActionSpace.__members__, 'Which action space to use') # pylint: disable=protected-access
flags.DEFINE_bool('use_feature_units', False, 'Whether to include feature units')
flags.DEFINE_bool('visualize', False, 'Whether to visualize the game')

flags.DEFINE_integer('num_episodes', 200000,'Number of episodes')
flags.DEFINE_integer('batch_size', 100, 'Batch size')
flags.DEFINE_float('gamma', 0.99, 'Gamma - reward discount factor')

# def _main(unused_args):
#   with tf.Session() as sess:
#     agent = VPGAgent(sess, batch_size=FLAGS.batch_size, gamma=FLAGS.gamma)

#     agent.screen_size = 84
#     agent.minimap_size = 64
#     agent.build_model((17, 84, 84), (17, 64, 64))

def _main(unused_args):
  with tf.Session() as sess:
    agent = VPGAgent(sess, batch_size=FLAGS.batch_size, gamma=FLAGS.gamma)

    with sc2_env.SC2Env(
      map_name=FLAGS.map,
      step_mul=FLAGS.game_step_mul,
      agent_interface_format=sc2_env.parse_agent_interface_format(
        feature_screen=FLAGS.feature_screen_size,
        feature_minimap=FLAGS.feature_minimap_size,
        rgb_screen=FLAGS.rgb_screen_size,
        rgb_minimap=FLAGS.rgb_minimap_size,
        action_space=FLAGS.action_space,
        use_feature_units=FLAGS.use_feature_units
      ),
      visualize=FLAGS.visualize,
    ) as env:
      run_loop.run_loop([agent], env, max_episodes=FLAGS.num_episodes)

if __name__ == '__main__':
  app.run(_main)
