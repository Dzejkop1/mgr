import numpy as np
import tensorflow as tf
import tensorflow.contrib.layers as layers
import tensorflow.keras.layers as K
from pysc2.lib import actions
from pysc2.lib import features

import time
import random

from absl import flags, app
FLAGS = flags.FLAGS

flags.DEFINE_float('reward_scale', 100.0, 'Reward scaling')
flags.DEFINE_float('beta', 0.1, 'Entropy loss coefficient')
flags.DEFINE_float('learning_rate', 1e-4, 'Learning rate')
flags.DEFINE_string('out_file', 'sc2.csv', 'The data output target file')
flags.DEFINE_bool('load_checkpoint', False, 'Whether or not to load the previous checkpoint')
flags.DEFINE_bool('normalize_rewards', False, 'Normalize rewards by subtracint mean and dividing by std')
flags.DEFINE_string('checkpoint_name', 'model.ckpt', 'Checkpoint name')
flags.DEFINE_string('session_file', '.session', 'Session checkpoint file name')

TENSOR_TYPE = tf.float32
NUMPY_TYPE = np.float32

def softmax(x):
  return np.exp(x) / np.sum(np.exp(x), axis=0)

def discount_rewards(rewards, gamma=0.99):
  r = np.array([gamma**i * rewards[i]
                for i in range(len(rewards))])
  r = r[::-1].cumsum()[::-1]

  if FLAGS.normalize_rewards:
    r = r - r.mean()
    if r.std() != 0:
      r = r / r.std()

  return r

def one_hot_encode(v, n):
  oh = np.zeros(n)

  oh[v] = 1.0

  return oh

class VPGAgent(object):
  def __init__(self, sess: tf.Session, batch_size: int, gamma: float):
    self.sess = sess
    self.batch_size = batch_size
    self.batch_counter: int = 0
    self.gamma = gamma

    self.n = 0
    self.i = 0
    self.summary_step_counter = 0

  def setup(self, obs_spec, action_spec):
    print ('VPGAgent.setup')
    self.screen_size = obs_spec['feature_screen'][1]
    self.minimap_size = obs_spec['feature_minimap'][1]
    self.build_model(obs_spec['feature_screen'], obs_spec['feature_minimap'])

    self.episode_observations = []
    self.episode_actions = []
    self.episode_rewards = []

    self.batch_observations = []
    self.batch_actions = []
    self.batch_rewards = []
    self.batch_total_rewards = []

  def build_model(self, feature_screen_shape, feature_minimap_shape):
    print('Building model')
    input_screen_shape = [None] + list(feature_screen_shape)
    input_minimap_shape = [None] + list(feature_minimap_shape)

    print('Inputs')
    self.input_screen = tf.placeholder(TENSOR_TYPE, shape=input_screen_shape, name='input_screen')
    self.input_minimap = tf.placeholder(TENSOR_TYPE, shape=input_minimap_shape, name='input_minimap')
    # self.value_target = tf.placeholder(TENSOR_TYPE, shape=[None], name='value_target')

    print('Convolutions')
    # pysc2 feature layers are shaped like [number of features, width, height] so a transposition into
    # [width, height, number of features] is necessary
    conv1 = K.Conv2D(4, 16, 1, padding='same', activation='relu')(tf.transpose(self.input_screen, [0, 2, 3, 1]))
    conv2 = K.Conv2D(3, 8, 1, padding='same', activation='relu')(conv1)
    conv3 = K.Conv2D(2, 4, 1, padding='same', activation='relu')(conv2)
    conv4 = K.Conv2D(1, 4, 1, padding='same', activation='sigmoid')(conv3)

    # sconv2 = K.Conv2D(4, 8, 2)(sconv1)

    # # same as above
    # mconv1 = K.Conv2D(2, 8, 2)(tf.transpose(self.input_minimap, [0, 2, 3, 1]))
    # mconv2 = K.Conv2D(4, 8, 2)(mconv1)

    # input_screen_flat = K.Flatten()(sconv2)
    # input_minimap_flat = K.Flatten()(mconv2)

    print('Concatenated input')
    # input_concatenated = tf.concat([input_screen_flat, input_minimap_flat], 1)

    conv_final = K.Flatten()(conv4)

    conv2_1 = K.MaxPool2D(8)(conv2)
    conv2_flat = K.Flatten()(conv2_1)

    fc = K.Dense(1024, activation='relu')(conv2_flat)
    fc = K.Dense(512, activation='relu')(fc)

    resolution_max = tf.expand_dims(tf.reduce_sum(tf.exp(conv_final), axis=1), 1)
    resolution_softmax = tf.exp(conv_final) / resolution_max

    print('Ouputs')
    self.output_screen = tf.exp(conv_final) / tf.expand_dims(tf.reduce_sum(tf.exp(conv_final), axis=1), 1)
    # self.output_screen = K.Dense(self.screen_size**2, activation='softmax')(conv_final)
    self.output_minimap = tf.exp(conv_final) / tf.expand_dims(tf.reduce_sum(tf.exp(conv_final), axis=1), 1)
    # self.output_minimap = K.Dense(self.minimap_size**2, activation='softmax')(conv_final)
    self.output_action_id = K.Dense(len(actions.FUNCTIONS), activation='softmax')(fc)

    print ('self.output_screen.shape: ', self.output_screen.shape)

    print('Summary')
    tf.summary.histogram('action_id_hist', self.output_action_id)
    tf.summary.histogram('output_minimap_hist', self.output_minimap)
    tf.summary.histogram('output_screen_hist', self.output_screen)

    print('Training placeholders')
    self.rewards = tf.placeholder(TENSOR_TYPE, shape=[None], name='rewards')

    self.selected_action_id = tf.placeholder(tf.float32, shape=[None, len(actions.FUNCTIONS)], name='selected_action_id')
    self.selected_screen = tf.placeholder(tf.float32, shape=[None, self.screen_size**2], name='selected_screen')
    self.selected_minimap = tf.placeholder(tf.float32, shape=[None, self.minimap_size**2], name='selected_minimap')

    self.was_screen_action = tf.placeholder(tf.float32, shape=[None], name='was_screen_action')
    self.was_minimap_action = tf.placeholder(tf.float32, shape=[None], name='was_minimap_action')

    print('Loss')
    # Clipping is introduced. As tf.log would produce NaN if it's input was 0
    selected_action_log_prob = tf.log(tf.clip_by_value(tf.reduce_sum(self.output_action_id * self.selected_action_id, axis=1), 1e-10, 1.))
    selected_screen_log_prob = tf.log(tf.clip_by_value(tf.reduce_sum(self.output_screen * self.selected_screen, axis=1), 1e-10, 1.))
    selected_minimap_log_prob = tf.log(tf.clip_by_value(tf.reduce_sum(self.output_minimap * self.selected_minimap, axis=1), 1e-10, 1.))

    advantage_estimate = tf.stop_gradient(self.rewards - tf.reduce_mean(self.rewards)) # A very basic advantage estimate

    total_log_prob = selected_action_log_prob + (self.was_screen_action * selected_screen_log_prob) + (self.was_minimap_action * selected_minimap_log_prob)

    entropy_loss  = tf.reduce_sum(self.output_action_id * tf.log(tf.clip_by_value(self.output_action_id, 1e-10, 1)))
    entropy_loss += tf.reduce_sum(self.output_minimap * tf.log(tf.clip_by_value(self.output_minimap, 1e-10, 1)))
    entropy_loss += tf.reduce_sum(self.output_screen * tf.log(tf.clip_by_value(self.output_screen, 1e-10, 1)))

    entropy_loss = -entropy_loss
    policy_loss = -tf.reduce_sum(total_log_prob * advantage_estimate)

    tf.summary.scalar('entropy_loss', entropy_loss)
    tf.summary.scalar('policy_loss', policy_loss)
    loss = policy_loss + (FLAGS.beta * entropy_loss)

    print('Average return')
    average_discounted_return = tf.reduce_mean(self.rewards, name='average_discounted_return')
    tf.summary.scalar('average_discounted_return', average_discounted_return)
    tf.summary.scalar('loss', loss)

    print('Optimizer')
    self.learning_rate = tf.placeholder(tf.float32, None, name='learning_rate')
    opt = tf.train.AdamOptimizer(self.learning_rate)

    self.train_op = opt.minimize(loss)

    self.run_metadata = tf.RunMetadata()
    self.merged = tf.summary.merge_all()

    self.train_writer = tf.summary.FileWriter('./log/vpg_%d' % int(time.time() / 60), graph=self.sess.graph)

    if FLAGS.load_checkpoint:
      self.load()
    else:
      self.sess.run(tf.global_variables_initializer())

  def reset(self):
    self.summary_step_counter = 0

  def save(self):
    saver = tf.train.Saver()
    saver.save(self.sess, FLAGS.checkpoint_name)

    with open(FLAGS.session_file, 'w') as f:
      f.write(FLAGS.checkpoint_name + '\n')
      f.write(str(self.i) + '\n')

  def load(self):
    with open(FLAGS.session_file, 'r') as f:
      checkpoint_name = f.readline().strip()
      session_number = int(f.readline().strip())

      self.i = session_number

      saver = tf.train.Saver()
      saver.restore(self.sess, checkpoint_name)

  def train(self):
    print ('Training')

    rewards = np.array(self.batch_rewards)

    selected_action_idx = np.array([o[0] for o in self.batch_actions])
    selected_action_id = np.array([one_hot_encode(o[0], len(actions.FUNCTIONS)) for o in self.batch_actions])
    selected_screen = np.array([one_hot_encode(o[1], self.screen_size ** 2) for o in self.batch_actions])
    selected_minimap = np.array([one_hot_encode(o[2], self.minimap_size ** 2) for o in self.batch_actions])

    input_screen = np.array([o[0] for o in self.batch_observations])
    input_minimap = np.array([o[1] for o in self.batch_observations])

    was_screen_action = np.zeros_like(selected_action_idx)
    was_minimap_action = np.zeros_like(selected_action_idx)

    for idx, act_id in enumerate(selected_action_idx):
      args = actions.FUNCTIONS[act_id].args
      arg_names = [a.name for a in args]

      if 'screen' in arg_names:
        was_screen_action[idx] = 1;
      if 'screen2' in arg_names:
        was_screen_action[idx] = 1
      if 'minimap' in arg_names:
        was_minimap_action[idx] = 1;

    run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
    summary, _ = self.sess.run([self.merged, self.train_op], feed_dict={
      self.selected_action_id: selected_action_id,
      self.selected_screen: selected_screen,
      self.selected_minimap: selected_minimap,
      self.input_screen: input_screen,
      self.input_minimap: input_minimap,
      self.rewards: rewards,
      self.was_minimap_action: was_minimap_action,
      self.was_screen_action: was_screen_action,

      self.learning_rate: FLAGS.learning_rate,
      },
      run_metadata=self.run_metadata,
      options=run_options)

    self.train_writer.add_summary(summary)
    self.train_writer.add_run_metadata(self.run_metadata, 'step %d' % self.n)

    self.batch_actions.clear()
    self.batch_observations.clear()
    self.batch_rewards.clear()

    with open(FLAGS.out_file, 'a') as f:
      import csv

      writer = csv.writer(f)
      writer.writerow([self.i, np.average(self.batch_total_rewards)])

      self.batch_total_rewards = []

    self.save()

  def discard(self):
    self.batch_actions = []
    self.batch_counter = 0
    self.batch_observations = []
    self.batch_rewards = []
    self.batch_total_rewards = []

    self.episode_actions = []
    self.episode_observations = []
    self.episode_rewards = []

  def step(self, obs):
    if self.n % 100 == 0:
      print ('Step %d' % self.n)

    feature_screen = np.array(obs.observation['feature_screen'])
    feature_minimap = np.array(obs.observation['feature_minimap'])

    available_actions = np.array(obs.observation['available_actions'])

    output_screen, output_minimap, output_action_id = self.sess.run([self.output_screen, self.output_minimap, self.output_action_id], feed_dict={
        self.input_screen: [feature_screen],
        self.input_minimap: [feature_minimap],
      },
    )

    self.n += 1

    output_screen = output_screen.ravel()
    output_minimap = output_minimap.ravel()
    output_action_id = output_action_id.ravel()

    screen_positions = np.arange(self.screen_size ** 2)
    minimap_positions = np.arange(self.minimap_size ** 2)

    try:
      chosen_screen_pos_idx = np.random.choice(screen_positions, p=output_screen)
    except:
      print('Screen out: ', output_screen)

    chosen_minimap_pos_idx = np.random.choice(minimap_positions, p=output_minimap)

    chosen_screen_pos = [chosen_screen_pos_idx // self.screen_size, chosen_screen_pos_idx % self.screen_size]
    chosen_minimap_pos = [chosen_minimap_pos_idx // self.minimap_size, chosen_minimap_pos_idx % self.minimap_size]

    available_action_probs = softmax(output_action_id[available_actions])
    act_id = np.random.choice(available_actions, p=available_action_probs)

    # if self.n % 100 == 0:
    #   print ('Available actions: ', available_actions)
    #   print ('Available action probs: ', available_action_probs)
    #   print ('Selected action: ', act_id)
    r = obs.reward
    if FLAGS.reward_scale:
      r *= FLAGS.reward_scale

    self.episode_actions.append([act_id, chosen_screen_pos_idx, chosen_minimap_pos_idx])
    self.episode_observations.append([feature_screen, feature_minimap])
    self.episode_rewards.append(r)

    if obs.last():
      self.batch_counter += 1
      self.i += 1

      self.batch_actions.extend(self.episode_actions)
      self.episode_actions.clear()

      self.batch_observations.extend(self.episode_observations)
      self.episode_observations.clear()

      self.batch_total_rewards.append(sum(self.episode_rewards))
      self.batch_rewards.extend(discount_rewards(self.episode_rewards, self.gamma))
      self.episode_rewards.clear()


      if self.batch_counter == self.batch_size:
        self.batch_counter = 0
        self.train()

    function = actions.FUNCTIONS[act_id]

    args = []
    for a in function.args:
      if a.name in ['screen', 'screen2']:
        args.append([chosen_screen_pos[1], chosen_screen_pos[0]])
      elif a.name == 'minimap':
        args.append([chosen_minimap_pos[1], chosen_minimap_pos[0]])
      else:
        args.append([0])

    return actions.FunctionCall(act_id, args)
