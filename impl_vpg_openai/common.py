import torch.nn as nn

PATH = '.model.torch'

def createModel(input_size=4, output_size=2):
    return nn.Sequential(
        nn.Linear(input_size, 16),
        nn.ReLU(inplace=True),
        nn.Linear(16, 64),
        nn.ReLU(inplace=True),
        nn.Linear(64, 16),
        nn.ReLU(inplace=True),
        nn.Linear(16, output_size),
        nn.ReLU(inplace=True),
        nn.Softmax(dim=-1),
    )