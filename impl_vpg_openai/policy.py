from typing import List
import gym
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import os.path
from common import PATH, createModel

def discount_rewards(rewards, gamma=0.99):
    r = np.array([gamma**i * rewards[i]
                  for i in range(len(rewards))])
    r = r[::-1].cumsum()[::-1]
    return r - r.mean()

def train(env, num_episodes, batch_size, lr, gamma):
    total_rewards = []

    model = createModel(
        input_size=env.observation_space.shape[0],
        output_size=env.action_space.n)

    if os.path.isfile(PATH):
        model.load_state_dict(torch.load(PATH))

    action_space = np.arange(env.action_space.n)
    opt = torch.optim.Adam(model.parameters(), lr)

    batch_counter = 1

    batch_states = []
    batch_rewards = []
    batch_actions = []

    for ep in range(num_episodes):
        episode_states = []
        episode_rewards = []
        episode_actions = []

        is_done = False
        obs = env.reset()
        while (not is_done):
            input_tensor = torch.FloatTensor(obs)
            action_probability_distribution = model(
                input_tensor).detach().numpy()

            action = np.random.choice(
                action_space, p=action_probability_distribution)

            episode_states.append(obs)

            obs, reward, is_done, _ = env.step(action)

            # env.render()

            episode_rewards.append(reward)
            episode_actions.append(action)

        batch_counter += 1
        batch_states.extend(episode_states)
        batch_rewards.extend(discount_rewards(episode_rewards, gamma))
        batch_actions.extend(episode_actions)
        total_rewards.append(sum(episode_rewards))

        if batch_counter == batch_size:
            opt.zero_grad()

            states_tensor = torch.FloatTensor(batch_states)
            rewards_tensor = torch.FloatTensor(batch_rewards)
            actions_tensor = torch.LongTensor(batch_actions)

            # print (states_tensor)
            policies = torch.log(model(states_tensor))

            selected_log_probs = rewards_tensor * policies[
                np.arange(len(actions_tensor)), actions_tensor]

            loss = -selected_log_probs.mean()

            loss.backward()
            opt.step()

            batch_states = []
            batch_rewards = []
            batch_actions = []
            batch_counter = 1

            average_score = np.mean(total_rewards[-10:])
            print ('Episode {}, average score is: {}'.format(
                ep, average_score
            ))
            print("\rEp: {} Loss: {:.2f}, Average of last 10: {:.2f}".format(
                ep + 1,
                loss.item(),
                average_score
            ), end="")

    torch.save(model.state_dict(), PATH)
    return total_rewards

if __name__ == "__main__":
    env = gym.make('CartPole-v0')

    rewards = train(env, 100, 10, 1e-2, 0.99)

    env.close()

    import matplotlib.pyplot as plt
    window = 10
    smoothed_rewards = [np.mean(rewards[i-window:i+1]) if i > window
                        else np.mean(rewards[:i+1]) for i in range(len(rewards))]

    plt.figure(figsize=(12,8))
    plt.plot(rewards)
    plt.plot(smoothed_rewards)
    plt.ylabel('Total Rewards')
    plt.xlabel('Episodes')
    plt.show()