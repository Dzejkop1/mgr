import gym
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from policy import createModel

PATH = 'model.torch'

env = gym.make('CartPole-v0')

model = createModel(
        input_size=env.observation_space.shape[0],
        output_size=env.action_space.n)

model.load_state_dict(torch.load(PATH))
model.eval()

action_space = np.arange(env.action_space.n)

is_done = False
obs = env.reset()
while not is_done:
    action_p = model(torch.FloatTensor(obs)).detach().numpy()
    action = np.random.choice(action_space, p=action_p)

    obs, _, is_done, _ = env.step(action)
    env.render()

env.close()
